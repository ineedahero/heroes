﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    [Serializable]
    public class Healer : Hero
    {
        private string placeForImg = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ8GKvCnWeR7LJUc_MXm8wkvdPupcdH3P9Leb1XzdSkJUkhpPXj";

        public Healer() : base()
        {
            this.attack = 4;
            this.defense = 5;
            this.healingPower = 10;
            this.imgUrlForChar = placeForImg;
        }

        public Healer(float atk, float def, float spd, string name, float heal, int mana) : base(atk, def, spd, name)
        {
            this.imgUrlForChar = placeForImg;
            this.healingPower = heal;
            this.mana = mana;
        }
    }
}
