﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Heroes
{
    [Serializable]
    public class Wizard : Hero
    {
        public float MagicAttack { get => this.magicAttack; set => this.magicAttack = value; }
        private string placeForImg = "https://cdn.instructables.com/FYG/AKTN/GLAMQ844/FYGAKTNGLAMQ844.LARGE.jpg?auto=webp&frame=1&fit=bounds";

        public Wizard() : base()
        {
            this.magicAttack = 20;
            this.imgUrlForChar = placeForImg;
        }

        public Wizard(float atk, float mgAtk, int mana, float def, float spd, string name) : base(atk, def, spd, name) 
        {
            this.magicAttack = mgAtk;
            this.imgUrlForChar = placeForImg;
            this.mana = mana;
        }

        public float AttackMagic()
        {
            return this.magicAttack;
        }
    }
}
