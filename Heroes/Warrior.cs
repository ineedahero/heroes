﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    [Serializable]
    public class Warrior : Hero
    {
        private bool canSmash = true;
        private string placeForImg = "https://cdn3.vectorstock.com/i/thumb-large/17/77/knight-vector-20411777.jpg";

        public Warrior() : base()
        {
            this.attack = 13;
            this.defense = 10;
            this.imgUrlForChar = placeForImg;
        }

        public Warrior(float atk, float def, float spd, string name) : base(atk, def, spd, name) 
        {
            this.imgUrlForChar = placeForImg;
        }

        public float Smash()
        {
            if (canSmash)
            {
                canSmash = false;
                return attack * 1.5f;
            }
            Console.WriteLine("Brute no can smash no more");
            return attack;
        }
    }
}
