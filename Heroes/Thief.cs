﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    [Serializable]
    public class Thief : Hero
    {
        private bool canBackStab = true;
        private string placeForImg = "https://i.pinimg.com/236x/10/dc/57/10dc573affe67a774db8badefacaeb0e--scratch-art-game-ideas.jpg";

        public Thief() : base()
        {
            this.attack = 12;
            this.defense = 6;
            this.imgUrlForChar = placeForImg;
        }
        public Thief (float atk, float def, float spd, string name) : base(atk, def, spd, name) 
        {
            this.imgUrlForChar = placeForImg;
        }

        public void Stealth()
        {
            if (canBackStab)
            {
                this.attack *= 1.5f;
                this.speed *= 1.5f;
            }
        }

        public float BackStab()
        {
            canBackStab = false;
            return attack * 1.25f;
        }

    }

}
