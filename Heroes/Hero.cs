﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Heroes
{
    [Serializable]
    public class Hero
    {
        protected string heroName = "Frodo";
        protected float hp = 100;
        protected int mana = 0;
        protected float attack = 10;
        protected float defense = 8;
        protected float speed = 8;
        protected float magicAttack = 0;
        protected float healingPower = 0;
        protected string imgUrlForChar = "";

        protected bool isHam = false;


        public string Name { get => heroName; set => heroName = value; }
        public float Attack {get => attack; set => attack = value;}
        public float Defense { get => defense; set => defense = value; }
        public float SpeedMpS { get => speed; set => speed = value; }
        public string ImgURLForChar { get => imgUrlForChar; set => imgUrlForChar = value; }

        public Hero() 
        {
        
        }

        public Hero(float atk, float def, float spd, string name)
        {
            this.attack = atk;
            this.defense = def;
            this.speed = spd;
            this.heroName = name;
        }

        public Hero(float def, float spd, bool hamOrNot)
        {
            this.isHam = hamOrNot;
            if (isHam)
            {
                this.hp = 500;
                this.attack = 100;
                this.magicAttack = 100;
            }
            this.defense = def;
            this.speed = spd;
        }

        public float Move()
        {
            return speed;
        }

        public float AttackMelee()
        {
            return attack;
        }

        public void SaveToFile(string fileToSaveTo)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(fileToSaveTo, FileMode.Create, FileAccess.Write);
            formatter.Serialize(stream, this);
            stream.Close();
        }

        public static Hero GetHero(string fileToGetHeroFrom)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(fileToGetHeroFrom, FileMode.Open, FileAccess.Read);
            Hero heroFromFile = (Hero) formatter.Deserialize(stream);
            stream.Close();
            return heroFromFile;
        }

    }
}
